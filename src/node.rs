use crate::{Compile, CompileResult, CustomElement};
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

type NodeRender = Box<dyn CustomElement>;

pub struct Node {
    pub id: Option<String>,
    pub tag: String,
    pub children: Vec<Node>,
    pub body: Rc<RefCell<NodeRender>>,
    pub attributes: HashMap<String, String>,
}

impl Node {
    pub fn new<K: Into<String>>(tag: K, body: NodeRender) -> Self {
        Self {
            id: None,
            tag: tag.into(),
            children: Vec::new(),
            body: Rc::new(RefCell::new(body)),
            attributes: HashMap::new(),
        }
    }

    pub fn append(&mut self, child: Node) {
        self.children.push(child);
    }

    pub fn get_attribute(&self, key: &str) -> Option<&String> {
        self.attributes.get(key)
    }

    pub fn set_attribute<K: Into<String>>(&mut self, key: K, value: K) {
        let key: String = key.into();
        let value: String = value.into();

        if key == "id" {
            self.id = Some(value.clone());
        }

        self.attributes.entry(key).or_insert(value);
    }

    pub fn flat_clone(&self, callback: &mut Vec<Node>) {
        self.flat().iter().for_each(|child| {
            callback.push(Node {
                id: child.id.clone(),
                tag: child.tag.clone(),
                body: child.body.clone(),
                attributes: child.attributes.clone(),
                children: child.children.clone(),
            });
            let render_result = child.body.borrow_mut().render();
            if let Some(node) = render_result.as_any().downcast_ref::<Node>() {
                callback.push(node.clone());
                node.flat_clone(callback);
            };
        });
    }

    fn flat(&self) -> Vec<&Node> {
        let mut children = vec![self];
        children.extend(self.children.iter().map(|node| node.flat()).flatten());

        children
    }
}

impl Compile for Node {
    fn compile(&self) -> CompileResult {
        let mut result = CompileResult::default();
        let render_result = self.body.borrow_mut().render();

        if let Some(compile_result) = render_result.as_any().downcast_ref::<CompileResult>() {
            result.extend(compile_result.clone())
        };

        result
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

impl std::fmt::Debug for Node {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            fmt,
            "Node {{ tag: {:?}, children: {:?}, attributes: {:?}:  }}",
            self.tag, self.children, self.attributes
        )
    }
}

impl Clone for Node {
    fn clone(&self) -> Self {
        // let mut render_children = Vec::new();
        // let render_result = self.body.borrow_mut().render();
        // let data: Vec<Node> = if let Some(node) = render_result.as_any().downcast_ref::<Node>() {
        //     return node.flat().iter().map(|item| *item.clone()).collect();
        // } else {
        //     Vec::new()
        // };

        Node {
            id: self.id.clone(),
            tag: self.tag.clone(),
            body: self.body.clone(),
            attributes: self.attributes.clone(),
            children: self.children.clone(),
        }
    }
}
