use super::style::Style;
use super::Document;
use std::any::Any;

pub trait CustomElement: Render + Any {
    fn as_any(&mut self) -> &mut dyn Any;
    fn update(&mut self);
}

pub trait Render {
    fn on_event(&mut self, event: UserEvent);
    fn render(&self) -> Box<dyn Compile>;
    fn before_render(&mut self, document: &Document);
    fn style(&mut self) -> Style;
}

pub trait Compile {
    fn compile(&self) -> CompileResult;
    fn as_any(&self) -> &dyn Any;
}

pub enum UserEvent {
    OnClick,
    OnTouch,
    OnHover,
}

#[derive(Clone, Debug)]
pub struct Vertex {
    pub position: [f32; 2],
}

#[derive(Clone, Debug)]
pub struct CompileResult {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u16>,
}

impl Compile for CompileResult {
    fn compile(&self) -> CompileResult {
        self.clone()
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }
}

impl CompileResult {
    pub fn extend(&mut self, result: CompileResult) {
        self.vertices.extend(result.vertices);
        self.indices.extend(result.indices);
    }
}

impl Default for CompileResult {
    fn default() -> Self {
        Self {
            vertices: Vec::new(),
            indices: Vec::new(),
        }
    }
}
