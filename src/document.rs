use super::definitions::{Compile, CompileResult, CustomElement};
use super::node::Node;

pub struct Document {
    nodes_buffer: Vec<Node>,
}

impl Document {
    pub fn new(node: Node) -> Self {
        let mut nodes: Vec<Node> = Vec::new();
        node.flat_clone(&mut nodes);
        Self {
            nodes_buffer: nodes,
        }
    }

    pub fn query_by_id<T: CustomElement>(&self, id: &str, callback: fn(&mut T)) {
        self.nodes_buffer.iter().for_each(|node| {
            if node.id == Some(String::from(id)) {
                let mut body = node.body.borrow_mut();
                if let Some(node) = body.as_any().downcast_mut::<T>() {
                    callback(node);
                };
            }
        });
    }

    pub fn compile(&self) -> CompileResult {
        let mut result = CompileResult::default();
        self.nodes_buffer.iter().for_each(|node| {
            {
                let mut body = node.body.borrow_mut();
                body.before_render(&self);
            }
            result.extend(node.compile());
        });
        result
    }
}
