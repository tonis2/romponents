use quote::{quote, ToTokens};

mod rsx;

use rsx::RawNode;

#[proc_macro_attribute]
pub fn element(
    _metadata: proc_macro::TokenStream,
    input: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let item: syn::ItemStruct = syn::parse(input).expect("failed to parse input");

    let ident = item.ident.clone();

    let output = quote! {

       #item

       impl CustomElement for #ident {
           fn as_any(&mut self) -> &mut dyn std::any::Any {
               self
           }


           fn update(&mut self) {}
       }

    };
    output.into()
}

#[proc_macro]
pub fn rsx(tokens: proc_macro::TokenStream) -> proc_macro::TokenStream {
    syn::parse2::<RawNode>(tokens.into())
        .unwrap()
        .into_token_stream()
        .into()
}
