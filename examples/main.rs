use romponents::{rsx, Document, Node};
mod widgets;

use widgets::{Container, Menuitem, Sidebar, Text};

fn main() {
    let node = rsx!(<container>
                        <sidebar/>
                        <sidebar/>
                    </container>);

    println!("{:?}", node.clone());
    let doc = Document::new(node);

    doc.query_by_id::<Menuitem>("test", |item| {
        item.set_width(200);
    });

    doc.query_by_id::<Menuitem>("test", |item| {
        println!("{:?}", item.get_width());
    });

    let result = doc.compile();
}
