use romponents::{
    element, rsx, Compile, CompileResult, CustomElement, Document, Node, Render, Style, UserEvent,
    Vertex,
};

#[element]
#[derive(Default)]
pub struct Container;

impl Render for Container {
    fn on_event(&mut self, event: UserEvent) {}

    fn style(&mut self) -> Style {
        Style::default()
    }

    fn before_render(&mut self, document: &Document) {}

    fn render(&self) -> Box<dyn Compile> {
        Box::new(CompileResult::default())
    }
}

#[element]
#[derive(Default)]

pub struct Menuitem {
    width: u32,
    height: u32,
}

impl Menuitem {
    pub fn set_width(&mut self, value: u32) {
        self.width = value;
    }

    pub fn get_width(&self) -> u32 {
        self.width
    }
}

impl Render for Menuitem {
    fn on_event(&mut self, event: UserEvent) {}

    fn style(&mut self) -> Style {
        Style::default()
    }

    fn before_render(&mut self, document: &Document) {}

    fn render(&self) -> Box<dyn Compile> {
        Box::new(rsx!(
            <container>
                <image src={"/assets/image.jpg"} />
                <text value={"test"}></text>
            </container>
        ))
    }
}

#[element]
#[derive(Default)]
pub struct Text {
    width: u32,
    height: u32,
}

impl Render for Text {
    fn on_event(&mut self, event: UserEvent) {}

    fn style(&mut self) -> Style {
        Style::default()
    }

    fn before_render(&mut self, document: &Document) {}

    fn render(&self) -> Box<dyn Compile> {
        let vertices: Vec<Vertex> = vec![
            Vertex {
                position: [5.0, 5.0],
            },
            Vertex {
                position: [6.0, 7.0],
            },
        ];
        Box::new(CompileResult {
            vertices,
            indices: Vec::new(),
        })
    }
}
#[element]
#[derive(Default)]
pub struct Sidebar {
    width: u32,
    height: u32,
}

impl Render for Sidebar {
    fn on_event(&mut self, event: UserEvent) {}

    fn style(&mut self) -> Style {
        Style::default()
    }

    fn before_render(&mut self, document: &Document) {}

    fn render(&self) -> Box<dyn Compile> {
        Box::new(rsx!(
            <container>
                <menuitem value={"first"}/>
                <menuitem id={"test"} value={"second"}/>
                <menuitem value={"third"}/>
            </container>
        ))
    }
}

#[element]
#[derive(Default)]
pub struct Image {
    width: u32,
    height: u32,
}

impl Render for Image {
    fn on_event(&mut self, event: UserEvent) {}

    fn style(&mut self) -> Style {
        Style::default()
    }

    fn before_render(&mut self, document: &Document) {}

    fn render(&self) -> Box<dyn Compile> {
        let vertices: Vec<Vertex> = vec![
            Vertex {
                position: [100.0, 100.0],
            },
            Vertex {
                position: [300.0, 200.0],
            },
        ];
        Box::new(CompileResult {
            vertices,
            indices: Vec::new(),
        })
    }
}
